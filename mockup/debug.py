'''
Handle debug hooks.
'''

import pdb
import logging

class Debugger(object):
    '''
    Contains debugger information.
    '''

    breakpoint_function = pdb.set_trace
    log_function = logging.debug

    def __init__(self):
        self.__hooks = {}

    def __call__(self, obj):
        '''
        Call debugger for an object.
        '''

        if obj in self.__hooks:
            for hook in self.__hooks[obj]:
                hook(obj)

    def add_hook(self, obj, func):
        '''
        Add a hook to an object.
        '''

        self.__hooks.setdefault(obj, [])
        self.__hooks[obj].append(func)

    def remove_hook(self, obj, func):
        '''
        Remove a hook from an object.
        '''

        if obj in self.__hooks:
            for index, val in enumerate(self.__hooks[obj]):
                if val == func:
                    break

            else:
                return

            del self.__hooks[obj][index]

    def unset_hooks(self, obj):
        '''
        Unset all hooks of an object.
        '''

        if obj in self.__hooks:
            del self.__hooks[obj]

    def clear_hooks(self):
        '''
        Clear all hooks.
        '''

        self.__hooks = {}

    def breakpoint(self, obj):
        '''
        Add a breakpoint to the object.
        '''

        self.add_hook(obj, lambda obj: self.breakpoint_function())

    def log(self, obj):
        '''
        Add a logging hook.
        '''

        self.add_hook(obj, self.__logger)

    def __logger(self, obj):
        '''
        Emit debug message when hitting a log hook.
        '''

        self.log_function('Debugger has just hit %r' % obj)

debugger = Debugger()
del Debugger
