from mockup.base import Base
from mockup.function import Function

RESERVED_KEYWORDS = set(('calls_completed', '__proxies__'))

class Object(Base):
    '''
    Emulate a class.
    '''

    def __init__(self, name=None, bases=(object,)):
        name = 'object-%d' % id(self) if name is None else name
        super(Object, self).__init__(name)

        self.__bases = bases
        self.__attributes = {}

    def create_proxy(self):
        @property
        def __calls_completed(self):
            for proxy in self.__proxies__:
                if not proxy.calls_completed:
                    return False

            return True

        proxies = []
        members = {}

        for key, value in self.__attributes.items():
            if isinstance(value, Function):
                proxy = value.proxy
                proxies.append(proxy)
                members[key] = proxy

            else:
                members[key] = value

        members['calls_completed'] = __calls_completed
        members['__proxies__'] = proxies

        return type(self.name, self.__bases, members)

    def member(self, key, value):
        '''
        Add a member to the class.
        '''

        self.__add_attribute(key, value)
        return self

    def members(self, **kwargs):
        '''
        Add multiple members to the class.
        '''

        for key, value in kwargs.items():
            self.member(key, value)

        return self

    def method(self, key, overwrite=False):
        '''
        Add a method to the class.
        '''

        if overwrite or key not in self.__attributes:
            method = Function('%s.%s' % (self.name, key))
            self.__add_attribute(key, method)

        return self.__attributes[key]

    def __add_attribute(self, key, value):
        '''
        Set an attribute (method or member) of the class.
        '''

        if key in RESERVED_KEYWORDS:
            raise RuntimeError("Attribute name %r is reserved" % key)

        self.__attributes[key] = value
