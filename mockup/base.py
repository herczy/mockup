'''
Module for base class.
'''

from debug import debugger

class Failure(AssertionError):
    '''
    Something failed with one of the mocks. This is not a failure in the mockup module but rather
    an exception signifying a failed condition.
    '''

class Base(object):
    '''
    Base class for mockup classes. A mockup class must contain a create_proxy() function
    that is used for creating a proxy entity (class, function, etc.).
    '''

    def __init__(self, name):
        self.__name = name

    @property
    def proxy(self):
        '''
        Return a newly created proxy entity.
        '''

        return self.create_proxy()

    @property
    def name(self):
        return self.__name

    def create_proxy(self):
        '''
        Create the proxy entity. Not implemented here.
        '''

        raise NotImplementedError('Base.create_proxy not implemented')

    def fail(self, reason):
        '''
        Raise a failure.
        '''

        debugger('fail')
        raise Failure(reason)

    def get_key(self, suffix=None):
        '''
        Return the key (with a possible suffix).
        '''

        res = self.__name
        if suffix is not None:
            res = res + '-' + str(suffix)

        return res
