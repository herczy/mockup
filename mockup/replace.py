class Replacement(object):
    def __init__(self):
        self.__replacements = []

    def replace(self, obj, attr, value):
        self.__replacements.append((obj, attr, value))

    def replace_multi(self, obj, **kwargs):
        for attr, value in kwargs.items():
            self.replace(obj, attr, value)

    def __enter__(self):
        self.__reverse = set()
        for obj, attr, value in self.__replacements.items():
            self.__reverse_append(obj, attr)
            setattr(obj, attr, value)

    def __exit__(self, *args):
        for revfunc in self.__reverse:
            revfunc()

        del self.__reverse

    def __reverse_append(self, obj, attr):
        if hasattr(obj, attr):
            oldval = getattr(obj, attr)
            self.__reverse.append(lambda: setattr(obj, attr, oldval))

        else:
            self.__reverse.append(lambda: delattr(obj, attr))

def replace_attribute(obj, **kwargs):
    '''
    Replace the given attributes in the object specified.
    '''

    res = Replacement()
    res.replace_multi(obj, **kwargs)

    return res
