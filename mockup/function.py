'''
Creates mockup for function calls.
'''

from mockup.base import Base
from mockup.debug import debugger

class FunctionProxy(object):
    '''
    Callable object acting as a function.
    '''

    def __init__(self, func):
        self.__func = func
        self.__index = 0

    @property
    def calls_completed(self):
        '''
        Returns true if the function was called the specified ammount of times.
        '''

        try:
            self.__func.get_sequence_item(self.__index, enable_fallback=False)

        except IndexError:
            return True

        return False

    def __call__(self, *args, **kwargs):
        self.__check_args(args, kwargs)

        try:
            current = self.__func.get_sequence_item(self.__index)

        except IndexError:
            debugger(self.__func.get_key('too-many-calls'))
            self.__func.fail("Too many calls were made to the function %r" % self.__func.name)

        self.__index += 1
        return current()

    def __get_argument(self, args, kwargs, pos=None, keyword=None):
        '''
        Get the argument at the given position and/or keyword.
        '''

        found = False
        res = None
        if pos is not None and pos < len(args):
            found = True
            res = args[pos]
            del args[pos]

        elif not found and keyword is not None and keyword in kwargs:
            found = True
            res = kwargs[keyword]
            del kwargs[keyword]

        if not found:
            debugger(self.__func.get_key('arg-not-found'))
            self.__func.fail("Argument %d/%s not found when calling %r" % (pos, keyword, self.__func.name))

        return res

    def __check_args(self, args, kwargs):
        '''
        Check existence and validity of arguments.
        '''

        dup_args = list(args)
        dup_kwargs = dict(kwargs)

        funcargs = self.__func.args
        for (pos, keyword), check_functions in funcargs.items():
            value = self.__get_argument(dup_args, dup_kwargs, pos=pos, keyword=keyword)

            for check in check_functions:
                check(value)

        if dup_args or dup_kwargs:
            debugger(self.__func.get_key('too-many-args'))
            self.__func.fail("Too many arguments added for function %r" % self.__func.name)

class Function(Base):
    def __init__(self, name=None):
        name = 'function-%d' % id(self) if name is None else name
        super(Function, self).__init__(name)

        self.__args = {}
        self.__sequence = []
        self.__default_return = None

    @property
    def args(self):
        '''
        Get a copy of the argument specifications.
        '''

        return dict(self.__args)

    def create_proxy(self):
        '''
        Create the function proxy item.
        '''

        return FunctionProxy(self)

    def get_sequence_item(self, index, enable_fallback=True):
        '''
        Return the item at the given index in the function return sequence.
        '''

        if index >= len(self.__sequence):
            if enable_fallback and self.__default_return is not None:
                return self.__default_return

            raise IndexError('Out of function sequence items')

        return self.__sequence[index]

    def returns(self, value, default=False):
        '''
        Specify the value to return when the function is called. If this function is used
        multiple times, the function will return the value of the nth call on the nth call
        to the function. E.g.:

            >>> f = Function('myfunc').returns(1).returns(2).create_proxy()
            >>> f()
            1
            >>> f()
            2
            >>> f()
            Exception
        '''

        return self.__add_sequence(lambda: value, default=default)

    def repeat(self, count):
        '''
        Repeat the last operation the given number of times. E.g.:
        
            >>> f = Function('myfunc').returns(1).repeat(2).create_proxy()
            >>> f()
            1
            >>> f()
            1
            >>> f()
            Exception
        '''

        top = self.__sequence.pop()
        for _ in range(count):
            self.__sequence.append(top)
        return self

    def raises(self, type, string='', default=False):
        '''
        Similar to returns(), but instead an exception is raised.
        '''

        def __raise():
            raise type(string)

        return self.__add_sequence(__raise, default=default)

    def check(self, pos=None, keyword=None, type=None, value=None, overwrite=False):
        '''
        Check the value and/or type of the given argument.
        '''

        check = []

        if pos is None and keyword is None:
            raise RuntimeError('Specify position and/or keyword')

        if type is not None:
            check_function = lambda arg: not isinstance(arg, type)
            check.append(self.__raise_function(check_function, TypeError, 'Mock %r got invalid argument (expected type: %r)' % (self.name, type)))

        if value is not None:
            check_function = lambda arg: arg != value
            check.append(self.__raise_function(check_function, ValueError, 'Mock %r got invalid argument (expected value: %r)' % (self.name, value)))

        key = (pos, keyword)
        if not overwrite:
            self.__args.setdefault(key, [])

        else:
            self.__args[key] = []

        self.__args[key].extend(check)

        return self

    def __raise_function(self, cond, type, string):
        '''
        Function call should raise an exception iff the condition function does not pass.
        '''

        def __function(*args, **kwargs):
            if cond(*args, **kwargs):
                debugger(self.get_key('arg-check'))
                raise type(string)

        return __function

    def __add_sequence(self, func, default=False):
        if default:
            self.__default_return = func

        else:
            self.__sequence.append(func)

        return self
