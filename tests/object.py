import unittest

from mockup.function import Function
from mockup.object import Object, RESERVED_KEYWORDS
from mockup.base import Failure

class TestObject(unittest.TestCase):
    def test_member(self):
        '''An object can have a member defined with member()'''

        obj = Object().member('member_name', 'member_value').proxy()
        self.assertEqual(obj.member_name, 'member_value')

    def test_members(self):
        '''An object can have a member defined with members()'''

        obj = Object().members(member_name='member_value', other_member='other_member_value').proxy()
        self.assertEqual(obj.member_name, 'member_value')
        self.assertEqual(obj.other_member, 'other_member_value')

    def test_method(self):
        '''An object can have a method defined'''

        obj = Object().members(member_name='member_value')
        method = obj.method('mymethod').returns(1)

        self.assertTrue(isinstance(method, Function))

        obj = obj.proxy()
        self.assertTrue(obj.mymethod(), 1)

    def test_method_overwrite(self):
        '''An object can have a method re-defined'''

        obj = Object().members(member_name='member_value')
        obj.method('mymethod').returns(0)
        obj.method('mymethod', overwrite=True).returns(1)

        obj = obj.proxy()
        self.assertTrue(obj.mymethod(), 1)

    def test_reserved_keyword(self):
        '''Method and member names cannot be some reserved ones.'''

        for key in RESERVED_KEYWORDS:
            self.assertRaises(RuntimeError, Object().member, key, 1)
            self.assertRaises(RuntimeError, Object().method, key)

    def test_method_call(self):
        '''Object method calls work like simple function calls (sanity test)'''

        obj = Object()
        obj.method('mymethod').returns('result', default=True)

        obj = obj.proxy()

        self.assertEqual(obj.mymethod(), 'result')
        self.assertRaises(Failure, obj.mymethod, 1, 2, 3, a=2)

    def test_calls_completed(self):
        '''Object can tell if all methods that should have been called were indeed called.'''

        obj = Object()
        obj.method('method0').returns(0)
        obj.method('method1').returns(0).returns(1)
        obj.method('method2').returns(0).returns(1).returns(2)

        obj = obj.proxy()

        self.assertFalse(obj.calls_completed)

        self.assertEqual(0, obj.method0())
        self.assertFalse(obj.calls_completed)

        self.assertEqual(0, obj.method1())
        self.assertFalse(obj.calls_completed)

        self.assertEqual(1, obj.method1())
        self.assertFalse(obj.calls_completed)

        self.assertEqual(0, obj.method2())
        self.assertFalse(obj.calls_completed)

        self.assertEqual(1, obj.method2())
        self.assertFalse(obj.calls_completed)

        self.assertEqual(2, obj.method2())
        self.assertTrue(obj.calls_completed)
