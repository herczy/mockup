import unittest

from mockup import replace

class TestReplace(unittest.TestCase):
    def test_replace_attribute(self):
        '''Attribute replacement works'''

        obj = type('someobj', (object,), dict(attribute=0))()
        with replace.replace_attribute(obj, attribute=3):
            self.assertEqual(obj.attribute, 3)

        self.assertEqual(obj.attribute, 0)

    def test_replace_attribute_nonexistent(self):
        '''Attribute replacement works for non-existent attributes'''

        obj = type('someobj', (object,), dict())()
        with replace.replace_attribute(obj, attribute=3):
            self.assertEqual(obj.attribute, 3)

        self.assertFalse(hasattr(obj, 'attribute'))
