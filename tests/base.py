import unittest

from mockup.base import Base, Failure

class TestBase(unittest.TestCase):
    def setUp(self):
        class Test(Base):
            def create_proxy(self):
                return 1

        self.test = Test

    def test_base(self):
        '''The Base mockup class works properly.'''

        self.assertEqual(self.test(None).proxy, 1)

    def test_create_proxy_unimplemented(self):
        '''The Base.create_proxy function is unimplemented.'''

        self.assertRaises(NotImplementedError, Base(None).create_proxy)

    def test_failure(self):
        '''The Base mockup class can emit a failure.'''

        self.assertRaises(Failure, self.test(None).fail, 'Nothing')
        self.assertRaises(AssertionError, self.test(None).fail, 'Nothing')

    def test_name(self):
        '''The Base mockup class contains a name attribute'''

        obj = self.test('myobj')
        self.assertEqual(obj.name, 'myobj')

    def test_get_key(self):
        '''The Base mockup class can return a possibly suffixed key'''

        obj = self.test('myobj')
        self.assertEqual('myobj', obj.get_key())
        self.assertEqual('myobj-hello', obj.get_key('hello'))
