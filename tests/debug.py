import unittest

from mockup.debug import debugger

class TestDebugger(unittest.TestCase):
    called = None

    def __called0(self, obj):
        self.called.setdefault(obj, [])
        self.called[obj].append(0)

    def __called1(self, obj):
        self.called.setdefault(obj, [])
        self.called[obj].append(1)

    def __trace(self, obj):
        def __tracer():
            self.called.setdefault(obj, [])
            self.called[obj].append('trace')

        return __tracer

    def __debug(self, obj):
        def __logger(msg):
            self.called.setdefault(obj, [])
            self.called[obj].append('debug')

        return __logger

    def setUp(self):
        self.called = {}

        debugger.clear_hooks()
        debugger.add_hook(0, self.__called0)
        debugger.add_hook(1, self.__called1)
        debugger.add_hook(2, self.__called0)
        debugger.add_hook(2, self.__called1)
        debugger.add_hook(2, self.__called1)

    def test_call(self):
        '''The call to the debugger calls all hooks in the order they were added.'''

        debugger(0)
        debugger(1)
        debugger(2)

        self.assertEqual(self.called[0], [0])
        self.assertEqual(self.called[1], [1])
        self.assertEqual(self.called[2], [0, 1, 1])

    def test_call_unknown_object(self):
        '''The debugger can be called without problems for unknown objects too without effect.'''

        debugger(6)
        self.assertEqual(self.called, {})

    def test_add_hook(self):
        '''New hooks can be added to the debugger for an object.'''

        debugger.add_hook(3, self.__called1)

        debugger(3)

        self.assertEqual(self.called[3], [1])

    def test_remove_hook(self):
        '''Individual hooks can be removed from the debugger.'''

        debugger.remove_hook(2, self.__called1)

        debugger(2)

        self.assertEqual(self.called[2], [0, 1])

    def test_remove_hook_ignore_missing_object(self):
        '''The debugger ignores if trying to remove a hook from a non-existent object.'''

        debugger.remove_hook(555, 666)

    def test_remove_hook_ignore_missing_hook(self):
        '''The debugger ignores if trying to remove a non-existent hook from an existing object.'''

        self.assertEqual(self.called, {})
        debugger.remove_hook(0, 666)

    def test_unset_hook(self):
        '''All hooks of an object can be unset.'''

        debugger.unset_hooks(0)

        debugger(0)

        self.assertEqual(self.called, {})

    def test_unset_hook_missing_object(self):
        '''The debugger ignores if trying to unset the hooks of a non-existent object.'''

        debugger.unset_hooks(555)

    def test_clear_hooks(self):
        '''All the hooks can be cleared.'''

        debugger.clear_hooks()

        debugger(0)
        debugger(1)
        debugger(2)

        self.assertEqual(self.called, {})

    def test_breakpoint(self):
        '''PDB breakpoints can be added to the debugger.'''

        debugger.breakpoint(5)

        old_breakpoint_function = debugger.breakpoint_function
        debugger.breakpoint_function = self.__trace(5)
        try:
            debugger(5)

        finally:
            debugger.breakpoint_function = old_breakpoint_function

        self.assertEqual(self.called, {5 : ['trace']})

    def test_log(self):
        '''Log hooks can be added to the debugger.'''

        debugger.log(5)

        old_log_function = debugger.log_function
        debugger.log_function = self.__debug(5)
        try:
            debugger(5)

        finally:
            debugger.log_function = old_log_function

        self.assertEqual(self.called, {5 : ['debug']})
