import unittest

from mockup.base import Failure
from mockup.function import Function

class TestFunction(unittest.TestCase):
    def test_returns(self):
        '''The function mock return value can be influenced'''

        func = Function().returns(1).returns(2).proxy
        self.assertEqual(func(), 1)
        self.assertEqual(func(), 2)
        self.assertRaises(Failure, func)

    def test_raises(self):
        '''The function mock can raise a value'''

        func = Function().raises(RuntimeError).raises(KeyError, 'a').proxy
        self.assertRaises(RuntimeError, func)
        self.assertRaises(KeyError, func)

    def test_check_args_type(self):
        '''The function mock can check arguments by type'''

        func = Function().check(0, type=int).check(1, keyword='a', type=str).returns(0, default=True).proxy

        self.assertEqual(func(0, a='a'), 0)
        self.assertEqual(func(0, 'a'), 0)
        self.assertRaises(TypeError, func, 'a', 0)
        self.assertRaises(TypeError, func, 0, 0)

    def test_check_args_equals(self):
        '''The function mock can check arguments by value'''

        func = Function().check(0, value=0).check(1, keyword='a', value=1).returns(0, default=True).proxy

        self.assertEqual(func(0, a=1), 0)
        self.assertEqual(func(0, 1), 0)
        self.assertRaises(ValueError, func, 1, 1)
        self.assertRaises(ValueError, func, 0, 0)

    def test_check_invalid(self):
        '''Raise an error if no position for the argument is given'''

        self.assertRaises(RuntimeError, Function().check)

    def test_check_overwrite(self):
        '''The function mock overwrite existing argument checks'''

        func = Function().check(0, value=0).check(0, value=1, overwrite=True).returns(0, default=True).proxy

        self.assertRaises(ValueError, func, 0)
        self.assertEqual(func(1), 0)

    def test_missing_arg(self):
        '''The function mock raises an exception if an argument is missing'''

        func = Function().check(0, value=0).returns(0).proxy

        self.assertRaises(Failure, func)

    def test_extra_arg(self):
        '''The function mock raises an exception if an unexpected argument is given'''

        func = Function().returns(0).proxy

        self.assertRaises(Failure, func, 1)

    def test_pass_condition(self):
        '''The function mock detects if it has been called the appropriate number of times.'''

        # Zero calls
        func = Function().proxy
        self.assertTrue(func.calls_completed)
        self.assertRaises(Failure, func)

        # One call
        func = Function().returns(1).proxy
        self.assertFalse(func.calls_completed)

        func()
        self.assertTrue(func.calls_completed)
        self.assertRaises(Failure, func)

        # Two call
        func = Function().returns(1).returns(2).proxy
        self.assertFalse(func.calls_completed)

        func()
        self.assertFalse(func.calls_completed)

        func()
        self.assertTrue(func.calls_completed)

        self.assertRaises(Failure, func)
        self.assertTrue(func.calls_completed)

    def test_function_name(self):
        '''The function name attribute'''

        func = Function()
        self.assertEqual(func.name, 'function-%d' % id(func))

        func = Function('test')
        self.assertEqual(func.name, 'test')

    def test_repeat(self):
        '''The sequence items can be repeated'''

        func = Function().returns(0).repeat(2).create_proxy()
        self.assertEqual(func(), 0)
        self.assertEqual(func(), 0)
        self.assertRaises(Failure, func)
